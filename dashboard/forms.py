"""
Dashboard forms.
"""
from django import forms
from .models import Shop, Category
from location_field.models.plain import PlainLocationField


class ShopForm(forms.ModelForm):
    city = forms.ModelChoiceField(queryset=Shop.objects.filter(parent=None))
    category = forms.ModelChoiceField(
        queryset=Category.objects.filter(parent=None),
        widget=forms.Select()
    )
    name = forms.CharField(forms.TextInput())
    location = PlainLocationField()
