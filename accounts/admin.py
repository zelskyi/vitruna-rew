from django.contrib import admin
from accounts.models import User, Owner, Seller
from django import forms
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField


class SellerCreationForm(forms.ModelForm):
    """A form for creating new seller. Includes all the required
    fields, plus a repeated password."""
    first_name = forms.CharField(label='Ім\'я')
    last_name = forms.CharField(label='Прізвище')
    password1 = forms.CharField(label='Пароль', widget=forms.PasswordInput)
    password2 = forms.CharField(
        label='Підтвердження паролю',
        widget=forms.PasswordInput
    )

    class Meta:
        model = Seller
        fields = ('email', 'role')

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        user = super(SellerCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class OwnerCreationForm(forms.ModelForm):
    """A form for creating new owners. Includes all the required
    fields, plus a repeated password."""
    first_name = forms.CharField(label='Ім\'я')
    last_name = forms.CharField(label='Прізвище')
    idn = forms.CharField(label='Ідентифікаційний')
    mobile = forms.CharField(label='Номер мобільного')
    password1 = forms.CharField(label='Пароль', widget=forms.PasswordInput)
    password2 = forms.CharField(
        label='Підтвердження паролю',
        widget=forms.PasswordInput
    )

    class Meta:
        model = User
        fields = ('email', 'role')

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        user = super(OwnerCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    first_name = forms.CharField(label='Ім\'я')
    last_name = forms.CharField(label='Прізвище')
    password1 = forms.CharField(label='Пароль', widget=forms.PasswordInput)
    password2 = forms.CharField(
        label='Підтвердження паролю',
        widget=forms.PasswordInput
    )

    class Meta:
        model = User
        fields = ('email', 'role')

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class SellerChangeForm(forms.ModelForm):
    """A form for updating sellers. Includes all the fields on
    the seller, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = Seller
        fields = ('email', 'password', 'role', 'parent')

    def clean_password(self):
        return self.initial["password"]


class OwnerChangeForm(forms.ModelForm):
    """A form for updating owners. Includes all the fields on
    the owner, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = Owner
        fields = (
            'email',
            'password',
            'first_name',
            'last_name',
            'idn', 'mobile',
            'tarrif', 'role',
            'is_active',
        )

    def clean_password(self):
        return self.initial["password"]


class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User
        fields = ('email', 'password', 'role', 'is_active', 'is_admin')

    def clean_password(self):
        return self.initial["password"]


class UserAdmin(BaseUserAdmin):
    form = UserChangeForm
    add_form = UserCreationForm

    list_display = ('email', 'role')
    list_filter = ('is_admin',)
    fieldsets = (
        (None, {'fields': ('email', 'first_name', 'last_name', 'password')}),
        ('Permissions', {'fields': ('is_admin',)}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': (
                'email',
                'first_name',
                'last_name',
                'role',
                'password1',
                'password2'
            )}
         ),
    )
    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()


class OwnerAdmin(BaseUserAdmin):
    form = OwnerChangeForm
    add_form = OwnerCreationForm

    list_display = ('email', 'tarrif', 'balance', 'mobile')
    list_filter = ('tarrif', 'balance')
    fieldsets = (
        (None, {'fields': (
            'email',
            'first_name',
            'last_name',
            'idn', 'mobile',
            'balance',
            'paid_to',
            'tarrif',
            'password')
            }
         ),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': (
                'email',
                'first_name',
                'last_name',
                'role',
                'idn',
                'mobile',
                'tarrif',
                'balance',
                'paid_to',
                'password1',
                'password2')}
         ),
    )
    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()


class SellerAdmin(BaseUserAdmin):
    form = SellerChangeForm
    add_form = SellerCreationForm

    list_display = ('email', 'first_name', 'last_name', 'parent')
    list_filter = ('parent',)
    fieldsets = (
        (None, {'fields': (
            'email',
            'first_name',
            'last_name',
            'parent',
            'role',
            'password')}
         ),
        ('Permissions', {'fields': ('is_admin',)}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': (
                'email',
                'first_name',
                'last_name',
                'role',
                'parent',
                'password1',
                'password2'
            )}
         ),
    )
    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()


admin.site.register(Seller, SellerAdmin)
admin.site.register(User, UserAdmin)
admin.site.register(Owner, OwnerAdmin)

admin.site.unregister(Group)
