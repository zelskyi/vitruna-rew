"""
Dashboard views
"""
from django.http import HttpResponseRedirect
from django.views.generic import FormView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse

from .models import Shop
from .forms import ShopForm


class SetupShop(FormView, LoginRequiredMixin):
    """
    Set up shom main view.
    """
    form_class = ShopForm()

    template_name = 'setup_shop.html'

    success_url = reverse('dashboard')
    login_url = reverse('login')

    def post(self, requset, *args, **kwargs):
        """
        Check if login user have shop.
        """
        if request.user.is_owner():
            try:
                have_shop = Shop.objects.get(owner=request.user.pk)
                if have_shop:
                    return HttpResponseRedirect(self.get_success_url())
            except:
                super(SetupShop).post(request, *args, **kwargs)
