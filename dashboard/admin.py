from django.contrib import admin
from dashboard.models import Category, City, Shop, Tarrifs
# Register your models here.


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'parent']

class TarrifAdmin(admin.ModelAdmin):
    list_display = ['name', 'tarrif_price']

class CityAdmin(admin.ModelAdmin):
    list_display = ['name', 'parent']


class ShopAdmin(admin.ModelAdmin):
    list_display = ['name', 'city']

admin.site.register(Tarrifs, TarrifAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(City, CityAdmin)
admin.site.register(Shop, ShopAdmin)
