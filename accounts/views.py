from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login


def login_view(request):
        email = request.POST.get('email', '')
        password = request.POST.get('password', '')
        user = authenticate(email=email, password=password)

        if user is not None:
            login(request, user)
            if login:
                if request.user.is_owner():
                    return redirect('/ownerlogin')
                if request.user.is_seller():
                    return redirect('/sellerlogin')
                else:
                    return redirect('/')
            else:
                login_error = 'Не вірно заповнений логін або пароль !'
                return render(
                        request,
                        template_name='accounts/login_new.html',
                        )
        else:
            if request.user.is_authenticated():
                if request.user.is_owner():
                    return redirect('/ownerlogin')
                if request.user.is_seller():
                    return redirect('/sellerlogin')
                else:
                    return redirect('/')
            else:
                return render(
                        request,
                        template_name='accounts/login_page_template.html'
                        )
