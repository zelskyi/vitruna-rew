module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {
            dev: {
                options: {
                    style: 'compressed'
                },
                files: {
                    './dist/css/style.css':'./vendor/Skeleton-Sass/scss/skeleton.scss'
                }
            }
        },
	cssmin: {
  options: {
    shorthandCompacting: false,
    roundingPrecision: -1
  },
  target: {
    files: {
      './dist/css/main.css': ['./dist/css/style.css']
    }
  }
},
        watch: {
            options: { livereload: true, },
            sass: {
                files: ['./dist/css/{,*/}*.css',
                        './vendor/Skeleton-Sass/scss/../*.scss'],
                tasks: ['sass:dev'],
                options: {
                    spawn: false,
                }
            },
            html: {
                files: ['./index.html']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-cssmin');	
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-sass');
    grunt.registerTask("default", ['sass','cssmin']);
   
};
