from django.db import models
from accounts.models import User
from dashboard.models import Shop
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.fields import GenericRelation
from star_ratings.models import Rating


class Review(models.Model):
    """
    Model for review from client
    """
    reviewer = models.ForeignKey(
        User
    )
    review_description = models.TextField(
        verbose_name=_('Відгук'),
        max_length=500,
        # TODO
        # Add star rating for review
        ratings = GenericRelation(Rating, related_query_name='rewviews')
    )
    shop = models.ForeignKey(
        Shop
    )

    def __str__(self):
        return self.review_description

    class Meta():
        verbose_name = _('Відгук')
        verbose_name_pluaral = _('Відгуки')
