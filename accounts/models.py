from django.db import models
# Create your models here.
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager

USER = 'User'
OWNER = 'Owner'
SELLER = 'Seller'
user_type_choice = (
        (USER, 'User'),
        (OWNER, 'Owner'),
        (SELLER, 'Seller'),
)


class SellerManager(BaseUserManager):
    """
    Manager for create  seller user
    """
    def create_user(
            self,
            email,
            first_name,
            last_name,
            parent,
            role,
            password=None
            ):
        """
        Create and saves a Seller with the given email, role,
        parent and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            role=SELLER,
            first_name=first_name,
            last_name=last_name,
        )
        user.parent = parent,
        user.set_password(password)
        user.save(using=self._db)
        return user


class OwnerManager(BaseUserManager):
    """
    Manager for create owner
    """
    def create_user(
            self,
            email,
            first_name,
            last_name,
            idn,
            tarrif,
            mobile,
            role,
            password=None
            ):
        """
        Creates and saves a Owner with the given email, role,
        idn, tarrif, mobile and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            role=OWNER,
            first_name=first_name,
            last_name=last_name,
        )
        user.mobile = mobile,
        user.idn = idn,
        user.tarrif = tarrif,
        user.set_password(password)
        user.save(using=self._db)
        return user


class CustomUserManager(BaseUserManager):
    """
    Custom manager for user
    """
    def create_user(self, email, first_name, last_name, role, password=None):
        """
        Creates and saves a User with the given email, role
        and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            role=role,
            first_name=first_name,
            last_name=last_name,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            email,
            password=password,
            role=USER,
            first_name='MEGA',
            last_name='GOD',
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    """
    Custom user model.
    """
    email = models.EmailField(
        verbose_name=_('Email'),
        max_length=255,
        unique=True
        )
    first_name = models.CharField(
        verbose_name=_('Імя'),
        max_length=20,
    )
    last_name = models.CharField(
        verbose_name=_('Прізвище'),
        max_length=30,
    )
    role = models.CharField(
        verbose_name=_('Роль'),
        max_length=100,
        choices=user_type_choice,
        default='User'
        )
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = CustomUserManager()

    USERNAME_FIELD = 'email'

    def get_full_name(self):
        # The user is identified by their email address
        return self.email

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def __str__(self):              # __unicode__ on Python 2
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin

    def _user_role_is(self, role):
        """
        Check if current user role matches to given.
        @:param role: user role.
        @:type role: int.
        @:rtype bool.
        @:return True if user matches given role. Else returns false.
        """
        if self.role == role:
            return True
        else:
            return False

    def is_owner(self):
        """
        Check is user is owner.
        """
        return self._user_role_is(OWNER)

    def is_seller(self):
        """
        Check is user is seller.
        """
        return self._user_role_is(SELLER)

    class Meta(AbstractBaseUser.Meta):
        swappable = 'AUTH_USER_MODEL'
        verbose_name = _('Користувач')
        verbose_name_plural = _('Користувачі')


class Owner(User):
    """
    Custom owner model child form custom user model.
    """
    idn = models.CharField(
        verbose_name=_('Ідентифікаційний код'),
        max_length=15,
        default=None,
        unique=True,
    )
    tarrif = models.ForeignKey(
        'dashboard.Tarrifs',
        verbose_name=_('Тариф')
    )
    mobile = models.CharField(
        verbose_name=_('Номер мобільного'),
        max_length=15,
        unique=True,
    )
    balance = models.PositiveIntegerField(default=0, verbose_name=_('Баланс'))
    paid_to = models.DateTimeField(verbose_name=_('Оплечений до'))

    objects = OwnerManager()

    def __init__(self, *args, **kwargs):
        self._meta.get_field('role').default = 'Owner'
        super(Owner, self).__init__(*args, **kwargs)

    class Meta():
        verbose_name = _('Підприємець')
        verbose_name_plural = _('Підприємці')


class Seller(User):
    parent = models.ForeignKey(
        Owner,
        verbose_name=_('Підприємець'),
    )

    objects = SellerManager()

    def __init__(self, *args, **kwargs):
        self._meta.get_field('role').default = 'Seller'
        super(Seller, self).__init__(*args, **kwargs)

    class Meta():
        verbose_name = _('Продавець')
        verbose_name_plural = _('Продавці')
