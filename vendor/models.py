from django.db import models
from accounts.models import Seller
from dashboard.models import Good
from django.utils.translation import ugettext_lazy as _


class Sale(models.Model):
    """
    Model for sale goods from seller
    """
    good = models.ForeignKey(
        Good
    )
    seller = models.ForeignKey(
        Seller
    )
    # TODO
    # Add DateTimeField and remove just DateField here.
    time_selling = models.DateTimeField(
        auto_add_now=True,
        editable=False
    )
    count = models.PositiveSmallIntegerField(
        default=1
    )

    def __str__(self):
        return self.seller

    class Meta():
        verbose_name = _('Дані продажу')
        verbose_name_plural = _('Таблиця продажів')
