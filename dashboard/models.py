from django.db import models
from accounts.models import Owner
from django.utils.translation import ugettext_lazy as _
from location_field.models.plain import PlainLocationField


class City(models.Model):
    """
    Model for city. And regions.
    """
    name = models.CharField(
        verbose_name=_('Назва міста'),
        max_length=40,
        default='Київ'
    )
    parent = models.ForeignKey(
        'self',
        verbose_name='Обласний центр',
        null=True,
        blank=True
    )
    location = PlainLocationField(
        based_fields=['name'],
        zoom=15,
        default='Київ'
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Місто')
        verbose_name_plural = _('Міста')


class Category(models.Model):
    """
    Model of category and subcategory.
    """
    name = models.CharField(
        verbose_name=_('Назва категорії'),
        max_length=100
    )
    parent = models.ForeignKey(
        'self',
        null=True,
        blank=True
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Категорія')
        verbose_name_plural = _('Категорії')


class Shop(models.Model):
    """
    Model for shop.
    """
    city = models.ForeignKey(
        City
    )
    name = models.CharField(
        verbose_name=_('Назва магазину'),
        max_length=70,
    )
    owner = models.ForeignKey(
        Owner
    )
    category = models.ForeignKey(
        Category
    )
    location = PlainLocationField(
        based_fields=['city'],
        zoom=15,
        default='Київ'
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Магазин')
        verbose_name_plural = _('Магазини')


class Good(models.Model):
    """
    Mode for dispaly good.
    """
    article = models.CharField(
        verbose_name=_('Артикул товару'),
        max_length=50
    )
    name = models.CharField(
        verbose_name=_('Назва товару'),
        max_length=150
    )
    shop = models.ForeignKey(
        Shop
    )
    category = models.ForeignKey(
        Category
    )
    image = models.ImageField(
        upload_to='media',
        # TODO
        # Check if i need this.
        blank=True
        # Right this
    )
    price = models.DecimalField(
        verbose_name=_('Ціна'),
        max_digits=10,
        decimal_places=2
    )
    description = models.TextField(
        verbose_name=_('Описання товару'),
        max_length=500,
        # TODO 
        # Check if i need these
        default='',
        # Right this
    )
    sale_price = models.DecimalField(
        verbose_name=_('Ціна при розпродажі'),
        max_digits=10,
        decimal_places=2
    )
    is_sale = models.BooleanField(
        verbose_name=_('Товар на розпродажы ?')
    )
    producer = models.CharField(
        verbose_name=_('Країна виробник'),
        max_length=20
    )
    count = models.PositiveSmallIntegerField(
        default=1
    )
    size = models.CharField(
        verbose_name=_('Розмір'),
        max_length=10
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Товар')
        verbose_name_plural = _('Товари')


class Tarrifs(models.Model):
    name = models.CharField(
        verbose_name=_('Назва'),
        max_length=20
    )
    latin_name = models.CharField(
        verbose_name=_('Назва латиницею'),
        max_length=20
    )
    seller_count = models.IntegerField(
        verbose_name=_('Кількість продавців')
    )
    shop_count = models.IntegerField(
        verbose_name=_('Кількість магазинів')
    )
    goods_count = models.IntegerField(
        verbose_name=_('Кількість товарів')
    )
    tarrif_price = models.IntegerField(
        verbose_name=_('Ціна тарифу')
    )
    on_sales = models.BooleanField(
        verbose_name=_('Показ в блоці "Розпродажі"')
    )
    is_donated = models.BooleanField(
        verbose_name=_('Фон')
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Тариф')
        verbose_name_plural = _('Тарифи')
